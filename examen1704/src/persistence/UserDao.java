package persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.User;

public class UserDao 
{
	  public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
	  public static final String DB_URL = "jdbc:mysql://localhost/examen1704";
	  public static final String DB_USER = "root";
	  public static final String DB_PASSWORD = "root";
	  private Connection connection = null;	
	
	  public UserDao() 
	  {
	        try 
	        {
	            Class.forName(DB_DRIVER).newInstance();
	        }
	        catch (Exception ex) 
	        {
	            System.out.println("Error -> Driver");
	        }
	        try
	        {
	            connection = DriverManager.getConnection(DB_URL + "?user=" + DB_USER + "&password=" + DB_USER);
	            System.out.println("Connected.");
	        }
	        catch (SQLException ex) 
	        {
	            System.out.println("Error -> Connection");
	            System.out.println("SQLException: " + ex.getMessage());
	        }
	    }
	  
	    @Override
	    protected void finalize() throws Throwable 
	    {
	        super.finalize();
	        if (connection != null) 
	        {
	            connection.close();        
	        }
	    }
	    
		public ArrayList <User> all() 
		{
		   ArrayList<User> User = new ArrayList<User>();
		   PreparedStatement stmt = null;
		   ResultSet rs = null;

		   String sql = "SELECT * FROM users";

		        try 
		        {
		            stmt = connection.prepareStatement(sql);
		            rs = stmt.executeQuery();
		            System.out.println("Query -> OK");
		            while (rs.next()) 
		            {
		            	User user = new User();
		          
		            	user.setId(rs.getInt("id"));
		            	user.setName(rs.getString("name"));
		            	User.add(user);
		            }
		        } 
		        catch (SQLException ex) 
		        {
		            System.out.println("SQLException: " + ex.getMessage());

		        } 
		        finally
		        {	
		            if (stmt != null) 
		            {
		                try 
		                {
							stmt.close();
						}
						catch (SQLException e) 
						{
							 System.out.println("SQLException: " + e.getMessage());
						}
		            }
		        }          
		        return User;
		    }
		
		   
		   public void insert(User user)
		   {
		        PreparedStatement stmt = null;

		        String sql = "INSERT INTO users (name) VALUES (?);";

		        try 
		        {
		            stmt = connection.prepareStatement(sql);
		            stmt.setString(1, user.getName());
		            stmt.executeUpdate() ;
		        } 
		        catch (SQLException ex) 
		        {
		            System.out.println("SQLException: " + ex.getMessage());
		        } 
		        finally 
		        {
		            if (stmt != null) 
		             {
		                try 
		                {
							stmt.close();
						} 
						catch (SQLException e) 
						{
							 System.out.println("SQLException: " + e.getMessage());
						}
		            }
		        }
		   }
		   
		   public void delete(int id)
		   {
		    	PreparedStatement stmt = null;

		        String sql = "DELETE FROM movies WHERE id = " + id + "";

		        try 
		        {
		            stmt = connection.prepareStatement(sql);
		            stmt.executeUpdate() ;
		        } 
		        catch (SQLException ex)
		         {
		            System.out.println("SQLException: " + ex.getMessage());
		        } 
		        finally 
		        {
		            if (stmt != null)
		            {
		                try 
		                {
							stmt.close();
						} 
						catch (SQLException e) 
						{
							 System.out.println("SQLException: " + e.getMessage());
						}
		            }
		        }
		    }
		   
		   public User getById(int id)
		   {
		        User movie = new User();
		        PreparedStatement stmt = null;
		        ResultSet rs = null;

		        String sql = "SELECT * FROM movies WHERE id = ?";

		        try 
		        {
		            stmt = connection.prepareStatement(sql);
		            stmt.setInt(1, id);
		            rs = stmt.executeQuery();
		            System.out.println("consulta realizada");

		            while (rs.next()) 
		            {
		            	movie.setId(rs.getInt("id"));
		            	movie.setName(rs.getString("title"));
		            }
		        } 
		        catch (SQLException ex)
		        {
		            System.out.println("SQLException: " + ex.getMessage());
		        }
		        finally
		        {
		        	 if (stmt != null) 
		        	 {
			                try 
			                {
								stmt.close();
							}
						    catch (SQLException e) 
						    {
								 System.out.println("SQLException: " + e.getMessage());
							}
			            }
		        }             
		        return movie;
		    }
	    
	    
}
