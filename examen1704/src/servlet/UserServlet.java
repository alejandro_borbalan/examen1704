package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.User;
import persistence.UserDao;


@WebServlet("/users")
public class UserServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;

    public UserServlet() 
    {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		request.setAttribute("users", new UserDao().all());

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/view/listUsers.jsp");
		dispatcher.forward(request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		User user = new User(null,request.getParameter("name"));
		
		new UserDao().insert(user);
		
		response.sendRedirect("/Examen1704/users");
	}

}
