<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="/examen1704/WEB-INF/css/default.css">
<title>Examen Java EE</title>
</head>
<body>


  <header>

    <div id="title">Examen Java EE. Curso 2017/2018</div>

    <nav>
      <ul>
        <li><a href="/examen1704/home">Inicio</a></li>
        <li><a href="/examen1704/users">Lista de Usuarios</a></li>
        <li><a href="/examen1704/users/create">Nueva</a></li>
      </ul>
    </nav>
  </header>
